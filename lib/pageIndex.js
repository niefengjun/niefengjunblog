/*
 * GET home page.
 */
const blog = require('./nedb/blog');

const config = require("./config.js").config;
const tool = require('./tool');

exports.index = async (req, res) => {
    let listli = '';
    const b = {};
    b.title = config.title;
    b.description = config.description;

    const xuanzhong = config.menu;
    let classname = '';

    let cSize = req.query.pageSize;

    if (!cSize) {
        cSize = 10;
    }

    let page = parseInt(req.params.page) || 1;

    classname = classname + '<ul class="nav pull-right navbar-nav">';
    classname = classname + '<li class="active" ><a href="/">首页</a></li>';
    for (let i = 0; i < xuanzhong.length; i++) {


        classname = classname + '<li><a href="' + xuanzhong[i].url + '">' + xuanzhong[i].name + '</a></li>';

    }
    classname = classname + '</ul>';
    const v = await blog.newlist();

    listli = listli + '<ul class="grove-list">';

    for (let i = 0; i < v.length; i++) {
        listli = listli + '<li><h5 class=\"media-heading\">';
        listli = listli + '<a href="/blog/' + v[i]._id + '.html">';
        listli = listli + '' + v[i].title + '';
        listli = listli + '</a>';
        listli = listli + '</li>';
        if (i == 10) {
            break;
        }
    }

    listli = listli + '</ul">';

    const data = await  blog.list();
    const toplist = [];
    if (data) {
        for (var i = 0; i < data.length; i++) {
            var top = data[i];
            if (data[i].content) {
                top.content = tool.delHtmlTag(data[i].content);
            }
            toplist.push(top);
        }
    }
    data.sort(tool.sortJSONArry('created', true, Date.parse));
    const ret = {
        page: page,
        perPage: cSize,
        total: data.length,
        items: tool.getPageData(toplist, cSize, page),
        pages: tool.getPageInfo(data.length, cSize).pageCount

    }
    res.render('index', {blog: b, title: config.title, listli: listli, classname: classname, paginate: ret});


};


const findOneid_yuedu = async (req, res) => {

    var id = req.params.id;
    const v = await  blog.findOneid_yuedu(id);
    res.send(0);
}

exports.findOneid_yuedu=findOneid_yuedu ;

